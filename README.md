# charcha.space

Charcha.Space is a project that is trying to re-invent the video/audio/screenshare based online conferencing system. 
Collaborations for the same are on.  

## WIP, more to be added.


### Technologies can be used

* [ ]  Codec based streaming
* [ ]  WebRTC (STUN, TURN and ICE Protocols) - Preferred, least amount of work, but stability on low bandwidth is an issue, but gives ability to control video quality


### Features :

* [ ]  Audio only calls
* [ ]  Video also calls
* [ ]  Chat by default
* [ ]  Screenshare (window, application, full-screen)
* [ ]  Collabrative editing  
* [ ]  Member listing (with controls to mute users and disable their video)

### Good to have

* [ ]  Whiteboard 
* [ ]  sequence diagrams/ flow diagrams (draw.io / sequencediagrams.org)
* [ ]  Video bridge (Youtube, facebook etc)
* [ ]  Provision for translations, close captions, simultaneous multiple translators
* [ ]  Echo cancellation, Noise reduction, 

### Bad haves

* [ ]  hackmd style wiki's (too extensive, too many features )


### Development and Deployment Guidelines

* [ ] Jhoom/eNota can be hosted independently by organzations, government and individuals
* [ ] Clients are interoperable using standard authentication processes
* [ ] Scalabilty - the role of resources and the role of architecture - limits documented 

* [ ] Standards. Protocols, Ports, RESTful apis, Bridges to other platforms
* [ ] Development open to issues, tracking, feature requests and contributions
* [ ] Development open to audit and source control process following GitHub or standard softare development platforms
* [ ] Scalability and testing open to 3rd parties.

### Refernces

* [ ] https://startups.meitystartuphub.in/#!/public/application/inc/5e92ec1269e3401cd7bc6db7
* [ ] Selective Forwarding Unit (SFU) https://jitsi.org/jitsi-videobridge/
* [ ] https://sylkserver.com/
